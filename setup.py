from setuptools import setup

setup(
    name='check_json',
    version='1.0.3',
    description='Check JSON plugin for nagios/icinga monitoring tools',
    url='https://gitlab.com/japtain_cack/check_json',
    author='Nathan Snow',
    author_email='japtain.cack@mimir-tech.org',
    license='CC-BY-ND-4.0',
    packages=['check_json'],
    install_requires=[
        'nagiosplugin',
        'pendulum',
        'flatten_json',
    ],
    zip_safe=False
)
